const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
let port = 8080;

const app = express();
app.use(bodyParser.json());

app.listen(port, "localhost", () => {
console.log("Serverul merge pe : " + port);

});

const connection = mysql.createConnection({
host: "localhost",
user: "root",
password: "",
database: "Masinutze"
 
})

connection.connect(err => {
    console.log("Baza de date conectata!");
    const sql =  "CREATE TABLE IF NOT EXISTS Masini(marca VARCHAR(25), cp INTEGER , culoare VARCHAR(15), capacitateMotor INTEGER)";
connection.query(sql , err => {

    if(err) throw err;
});

});

app.post("/masina" , (req , res) => {

const masina = {
    marca: req.body.marca ,
    cp: req.body.cp, 
    culoare: req.body.culoare,
    capacitateMotor: req.body.capacitateMotor 
}
  let errors = [];
   if(!masina.marca || !masina.cp || !masina.culoare || !masina.capacitateMotor ){
 errors.push("Nu ati introdus unul sau mai multe campuri")
  }

if (!masina.marca.match("[^0-9.]")){
    errors.push("Marca masinii trebuie sa contina doar litere")

}
if(errors.length === 0){
    const insert = "INSERT INTO Masini(marca , cp , culoare , capacitateMotor) VALUES ('"+masina.marca+ "', '"+masina.cp+ "' , '"+masina.culoare+ "' , '"+masina.capacitateMotor+ "')";

connection.query(insert , err => {
if(err) throw err;
else {
    console.log("Masina adaugata in bd.");
    res.status(200).send({message: "Ai introdus o masina in bd"})
}

});

}
else {
    console.log("Eroare de server!");
    res.status(500).send(errors);


}

});
app.get("/masini" , (req, res) => {
    const sql = "SELECT * FROM Masini";
    connection.query(sql, (err, result) => {
        if(err) throw err;
        res.status(200).send(result);
    })
})